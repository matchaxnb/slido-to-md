# How to make the aggregated-slido.json

Open your browser's inspector.

Add a filter in the network tab for analytics.

Go to the analytics page of each of your polls.

Copy the outcome of the "analytics-v2" route from the browser to a file.

Then just run
`cat yourfile | jq -s > aggregated-slido.json`

# Usage

`cat aggregated-slido.json | python slido-to-md.py`
