#!/usr/bin/env python

import json
import sys

parsed = json.load(sys.stdin)

polls = [f['polls'] for f in parsed]
for poll in polls:
  for item in poll['items']:
    activeDate = item['dateActive'][0:10]
    print(f"## quiz from {activeDate}")
    for question in item['questions']:
      print(f"### {question['title']}")
      for answer in question['options']:
        is_correct = answer['isCorrect']
        is_correct_str = '**' if is_correct else ''
        print(f' - {is_correct_str}{answer["label"]}{is_correct_str}')
      print('-----')
